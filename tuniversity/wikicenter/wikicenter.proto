syntax = "proto3";

option go_package = "gitlab.com/henry0475/protobufs/tuniversity/wikicenter/wikicenterpb";

service WikiCenterService {
    rpc GetWiki (GetWikiRequest) returns (GetWikiReply) {}
    rpc GetWikis(GetWikisRequest) returns(GetWikisReply) {}
    rpc GetAuthors(GetAuthorsRequest) returns (GetAuthorsReply) {}
    rpc GetWBadges(GetWBadgesRequest) returns (GetWBadgesReply) {}

    rpc GetOperationStatus(GetOperationStatusRequest) returns (GetOperationStatusReply) {}

    rpc SupportWiki(SupportWikiRequest) returns (SupportWikiReply) {}
    rpc DisappointWiki(DisappointWikiRequest) returns (DisappointWikiReply) {}
    
    rpc AddWiki(AddWikiRequest) returns(AddWikiReply) {}
    rpc GetIDByTitle(GetIDByTitleRequest) returns(GetIDByTitleReply) {}

    rpc SubmitModification(SubmitModificationRequest) returns(SubmitModificationReply) {}
    rpc AllowedModification(AllowedModificationRequest) returns(AllowedModificationReply) {}
    rpc AllowedManagement(AllowedManagementRequest) returns (AllowedManagementReply) {}

    rpc SearchWikis(SearchWikisRequest) returns (SearchWikisReply) {}
}

message DisappointWikiRequest {
    string uid = 1;
    string wid = 2;
}
message DisappointWikiReply {
    int64 status = 1;
}

message GetWBadgesRequest {
    string wid = 1;
}
message GetWBadgesReply {
    int64 status = 1;
    repeated wbadge badges = 2;
}

message wbadge {
    int64 id = 1;
    string name = 2;
    string link = 3;
    int64 priority = 4;
}

message SearchWikisRequest {
    string key = 1;
    repeated string tags = 2;
}
message SearchWikisReply {
    int64 status = 1;
    repeated wiki wikis = 2;
}

message AllowedManagementRequest {
    string uid = 1;
    string wid = 2;
}
message AllowedManagementReply {
    int64 status = 1;
}

message AllowedModificationRequest {
    string uid = 1;
    string wid = 2;
}
message AllowedModificationReply{
    int64 status = 1;
}

message SupportWikiRequest {
    string wid = 1;
    string uid = 2;
}
message SupportWikiReply {
    int64 status = 1;
}

message GetIDByTitleRequest {
    string title = 1;
}
message GetIDByTitleReply{
    int64 status = 1;
    string id = 2;
}

message GetWikisRequest {
    repeated string tags = 1;
    int64 condition = 2;
    int64 page = 3;
}
message GetWikisReply {
    int64 status = 1;
    repeated wiki wikis = 2;
}

message SubmitModificationRequest {
    string uid = 1;
    wiki wiki = 2;
    string requestContent = 3;
}
message SubmitModificationReply {
    int64 status = 1;
}

message AddWikiRequest {
    string uid = 1;
    repeated string tags = 2;
    string title = 3;
    string content = 4;
}
message AddWikiReply{
    int64 status = 1;
    string id = 2;
}

message GetAuthorsRequest {
    string wid = 1;
}

message GetAuthorsReply {
    int64 status = 1;
    repeated author authors = 2;
}

message author {
    string uid = 1;
    string avatar = 2;
    string name = 3;
}

message metadata {
    int64 createTime = 1;
    int64 lastUpdateTime = 2;
    int64 version = 3;
    int64 supports = 4;
    int64 views = 5;
    int64 disappointments = 6;
}

message operationStatus {
    bool supported = 1;
    bool disappointed = 2;
}

message GetOperationStatusRequest {
    string uid = 1;
    string wid = 2;
}
message GetOperationStatusReply {
    int64 status = 1;
    operationStatus operationStatus = 2;
}

message wiki {
    string id = 1;
    string title = 2;
    string abstract = 3;
    string content = 4;
    repeated string tags = 5;
    int64 status = 6;
    author owner = 7;
    author maintainer = 8;
    author writer = 9;
    repeated author coAuthors = 10;
    metadata metadata = 11;
}

message GetWikiRequest {
    string wid = 1;
    // if the version is zero, the latest version of wiki will be returned
    int64 version = 2;
}
message GetWikiReply {
    int64 status = 1;
    wiki wiki = 2;
}