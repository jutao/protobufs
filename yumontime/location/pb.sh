#!/bin/bash

COMMAND=${1^^}
SERVICE_NAME=${2}

CURRENT_PATH=`pwd`
BASENAME=`basename ${CURRENT_PATH}`

if [[ "${SERVICE_NAME}" == "" ]]; then
    SERVICE_NAME=${BASENAME}
fi

if [[ "${COMMAND}" == "" ]]; then
    echo -e "Invalid command."
    exit -1
fi

function update_grpc(){
    FILE_NAME="${SERVICE_NAME}.proto"
    if [ ! -f "${FILE_NAME}" ]; then
        echo -e "${FILE_NAME} does not exist in ${CURRENT_PATH}"
        exit -1
    fi
    echo -e "*** [GRPC] Updating ${FILE_NAME} ..."
    /usr/local/bin/protoc --go_out=. \
        --go_opt=paths=source_relative \
        --go-grpc_out=. \
        --go-grpc_opt=paths=source_relative ${FILE_NAME}
    echo -e "*** Updated! "
}

function update_gw(){
    FILE_NAME="${SERVICE_NAME}.proto"
    if [ ! -f "${FILE_NAME}" ]; then
        echo -e "${FILE_NAME} does not exist in ${CURRENT_PATH}"
        exit -1
    fi
    echo -e "*** [gRPC-Gateway] Updating ${FILE_NAME} ..."
    /usr/local/bin/protoc --grpc-gateway_out . \
        --grpc-gateway_opt logtostderr=true \
        --grpc-gateway_opt paths=source_relative \
        --grpc-gateway_opt generate_unbound_methods=true ${FILE_NAME}
    echo -e "*** Updated! "
}

case ${COMMAND} in
    # update the current protobuf protocol
    "UPDATE")
        update_grpc
        update_gw
    ;;

    *)
        echo -e "DOES NOT SUPPORT: ${COMMAND}" 
    ;;
esac