// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package fpdfservicepb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// FpdfServiceClient is the client API for FpdfService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FpdfServiceClient interface {
	TransToDoc(ctx context.Context, in *TransToDocRequest, opts ...grpc.CallOption) (*TransToDocReply, error)
	ReceivedCallback(ctx context.Context, in *ReceivedCallbackRequest, opts ...grpc.CallOption) (*ReceivedCallbackReply, error)
	GetResult(ctx context.Context, in *GetResultRequest, opts ...grpc.CallOption) (*GetResultReply, error)
}

type fpdfServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewFpdfServiceClient(cc grpc.ClientConnInterface) FpdfServiceClient {
	return &fpdfServiceClient{cc}
}

func (c *fpdfServiceClient) TransToDoc(ctx context.Context, in *TransToDocRequest, opts ...grpc.CallOption) (*TransToDocReply, error) {
	out := new(TransToDocReply)
	err := c.cc.Invoke(ctx, "/FpdfService/TransToDoc", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpdfServiceClient) ReceivedCallback(ctx context.Context, in *ReceivedCallbackRequest, opts ...grpc.CallOption) (*ReceivedCallbackReply, error) {
	out := new(ReceivedCallbackReply)
	err := c.cc.Invoke(ctx, "/FpdfService/ReceivedCallback", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fpdfServiceClient) GetResult(ctx context.Context, in *GetResultRequest, opts ...grpc.CallOption) (*GetResultReply, error) {
	out := new(GetResultReply)
	err := c.cc.Invoke(ctx, "/FpdfService/GetResult", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FpdfServiceServer is the server API for FpdfService service.
// All implementations must embed UnimplementedFpdfServiceServer
// for forward compatibility
type FpdfServiceServer interface {
	TransToDoc(context.Context, *TransToDocRequest) (*TransToDocReply, error)
	ReceivedCallback(context.Context, *ReceivedCallbackRequest) (*ReceivedCallbackReply, error)
	GetResult(context.Context, *GetResultRequest) (*GetResultReply, error)
	mustEmbedUnimplementedFpdfServiceServer()
}

// UnimplementedFpdfServiceServer must be embedded to have forward compatible implementations.
type UnimplementedFpdfServiceServer struct {
}

func (UnimplementedFpdfServiceServer) TransToDoc(context.Context, *TransToDocRequest) (*TransToDocReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method TransToDoc not implemented")
}
func (UnimplementedFpdfServiceServer) ReceivedCallback(context.Context, *ReceivedCallbackRequest) (*ReceivedCallbackReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReceivedCallback not implemented")
}
func (UnimplementedFpdfServiceServer) GetResult(context.Context, *GetResultRequest) (*GetResultReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetResult not implemented")
}
func (UnimplementedFpdfServiceServer) mustEmbedUnimplementedFpdfServiceServer() {}

// UnsafeFpdfServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FpdfServiceServer will
// result in compilation errors.
type UnsafeFpdfServiceServer interface {
	mustEmbedUnimplementedFpdfServiceServer()
}

func RegisterFpdfServiceServer(s grpc.ServiceRegistrar, srv FpdfServiceServer) {
	s.RegisterService(&FpdfService_ServiceDesc, srv)
}

func _FpdfService_TransToDoc_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TransToDocRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpdfServiceServer).TransToDoc(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/FpdfService/TransToDoc",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpdfServiceServer).TransToDoc(ctx, req.(*TransToDocRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpdfService_ReceivedCallback_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReceivedCallbackRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpdfServiceServer).ReceivedCallback(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/FpdfService/ReceivedCallback",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpdfServiceServer).ReceivedCallback(ctx, req.(*ReceivedCallbackRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FpdfService_GetResult_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetResultRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FpdfServiceServer).GetResult(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/FpdfService/GetResult",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FpdfServiceServer).GetResult(ctx, req.(*GetResultRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// FpdfService_ServiceDesc is the grpc.ServiceDesc for FpdfService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var FpdfService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "FpdfService",
	HandlerType: (*FpdfServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "TransToDoc",
			Handler:    _FpdfService_TransToDoc_Handler,
		},
		{
			MethodName: "ReceivedCallback",
			Handler:    _FpdfService_ReceivedCallback_Handler,
		},
		{
			MethodName: "GetResult",
			Handler:    _FpdfService_GetResult_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "qdlxyh/fpdfservice/fpdfservice.proto",
}
